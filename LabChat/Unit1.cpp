//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TLabChat *LabChat;
//---------------------------------------------------------------------------
__fastcall TLabChat::TLabChat(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLabChat::FormShow(TObject *Sender)
{
	edName->Text = L"������� " + IntToStr(Random(99));
	edValue->Text = L"������! ��� ����?";
    ttm->AutoConnect();
}
//---------------------------------------------------------------------------
void __fastcall TLabChat::ttmPairedFromLocal(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo)

{
    me->Lines->Add("PairedFromLocal: " + AManagerInfo.ManagerText + " - " + AManagerInfo.ManagerIdentifier);
}
//---------------------------------------------------------------------------
void __fastcall TLabChat::BtnSentClick(TObject *Sender)
{
	for (int i = 0; i < ttp->ConnectedProfiles->Count; i++){
		ttp->SendString(ttp->ConnectedProfiles->Items[i], edName->Text, edValue->Text);
  	}
}
//---------------------------------------------------------------------------
void __fastcall TLabChat::ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    me->Lines->Add(AResource->Hint + ": " + AResource->Value.AsString);
}
//---------------------------------------------------------------------------
