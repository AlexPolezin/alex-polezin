//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class TLabChat : public TForm
{
__published:	// IDE-managed Components
	TMemo *me;
	TLayout *Layout1;
	TLayout *Layout2;
	TEdit *edName;
	TEdit *edValue;
	TButton *BtnSent;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TStyleBook *StyleBook1;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ttmPairedFromLocal(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo);
	void __fastcall BtnSentClick(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);


private:	// User declarations
public:		// User declarations
	__fastcall TLabChat(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLabChat *LabChat;
//---------------------------------------------------------------------------
#endif
