//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buCloseClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction2->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction3->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button10Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
	MediaPlayer1->Stop();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button9Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction1->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction1_1->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button6Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction4->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button11Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction2->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction2_1->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button13Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction3->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button5Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction3_1->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button15Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction6->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button16Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction5_1->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button8Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction5->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPlayClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMission->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button20Click(TObject *Sender)
{
	ShowMessage(L"����������� ����� � ��������� ����������");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button17Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiAction1->Index);
	MediaPlayer1->Play();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab=tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"�������� ������� ��������� 151-362");
}
//---------------------------------------------------------------------------
