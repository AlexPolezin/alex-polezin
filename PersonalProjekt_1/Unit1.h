//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiMission;
	TTabItem *tiAction1;
	TImage *Image1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TTabItem *tiAction2;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button3;
	TButton *Button4;
	TImage *Image2;
	TTabItem *tiAction3;
	TGridPanelLayout *GridPanelLayout3;
	TButton *Button5;
	TButton *Button6;
	TImage *Image3;
	TTabItem *tiAction4;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button7;
	TButton *Button8;
	TImage *Image4;
	TTabItem *tiAction1_1;
	TGridPanelLayout *GridPanelLayout5;
	TButton *Button9;
	TButton *Button10;
	TImage *Image5;
	TTabItem *tiAction2_1;
	TGridPanelLayout *GridPanelLayout6;
	TButton *Button11;
	TButton *Button12;
	TImage *Image6;
	TTabItem *tiAction3_1;
	TGridPanelLayout *GridPanelLayout7;
	TButton *Button13;
	TButton *Button14;
	TImage *Image7;
	TTabItem *tiAction5;
	TGridPanelLayout *GridPanelLayout8;
	TButton *Button15;
	TButton *Button16;
	TImage *Image8;
	TTabItem *tiAction6;
	TGridPanelLayout *GridPanelLayout9;
	TButton *Button18;
	TImage *Image9;
	TTabItem *tiAction5_1;
	TGridPanelLayout *GridPanelLayout10;
	TButton *Button19;
	TImage *Image10;
	TGridPanelLayout *GridPanelLayout11;
	TButton *Button17;
	TButton *Button20;
	TButton *Button21;
	TStyleBook *StyleBook1;
	TButton *buPlay;
	TButton *buAbout;
	TGridPanelLayout *GridPanelLayout12;
	TLabel *Label2;
	TButton *Button22;
	TMediaPlayer *MediaPlayer1;
	TGridPanelLayout *GridPanelLayout13;
	void __fastcall buCloseClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall Button16Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall Button20Click(TObject *Sender);
	void __fastcall Button17Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
