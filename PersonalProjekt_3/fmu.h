//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiSpisok;
	TTabItem *tiInfo;
	TListView *lv;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TToolBar *tb1;
	TButton *buMenu;
	TLabel *laList;
	TGridPanelLayout *gpInfo;
	TMemo *meInfo;
	TToolBar *tb2;
	TButton *buSpisok;
	TLabel *laName;
	TGridPanelLayout *gpMenu;
	TLabel *laMain;
	TButton *buSpiok;
	TButton *buAbout;
	TButton *buClose;
	TStyleBook *StyleBook1;
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buMenuClick(TObject *Sender);
	void __fastcall buSpisokClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buCloseClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
