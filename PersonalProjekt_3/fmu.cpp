//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tiInfo->Index);
	laName->Text = dm->taListNAME->Value;
	meInfo->Text = dm->taListINFO->Value;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMenuClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSpisokClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiSpisok->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage(L"������ �������� ������ 151-362 �������� ����������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCloseClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

