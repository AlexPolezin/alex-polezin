object dmAAA: TdmAAA
  OldCreateOrder = False
  Height = 293
  Width = 352
  object CitisConnection: TSQLConnection
    ConnectionName = 'citis'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\Alex\Desktop\PersonalProjekt_2\inc\CITYS.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = CitisConnectionAfterConnect
    Connected = True
    Left = 70
    Top = 52
  end
  object taCitys: TSQLDataSet
    Active = True
    CommandText = 'CITYS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = CitisConnection
    Left = 70
    Top = 100
    object taCitysID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taCitysNAME: TWideStringField
      FieldName = 'NAME'
      Size = 400
    end
    object taCitysDATE: TIntegerField
      FieldName = 'DATE'
    end
    object taCitysDISCIPTION: TWideStringField
      FieldName = 'DISCIPTION'
      Size = 20000
    end
    object taCitysIMG: TBlobField
      FieldName = 'IMG'
      Size = 1
    end
  end
  object dspCitys: TDataSetProvider
    DataSet = taCitys
    Left = 64
    Top = 152
  end
end
