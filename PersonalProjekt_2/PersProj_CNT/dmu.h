//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsCitys;
	TIntegerField *cdsCitysID;
	TWideStringField *cdsCitysNAME;
	TIntegerField *cdsCitysDATE;
	TWideStringField *cdsCitysDISCIPTION;
	TBlobField *cdsCitysIMG;
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TdmAAAClient* FdmAAAClient;
	TdmAAAClient* GetdmAAAClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmAAAClient* dmAAAClient = {read=GetdmAAAClient, write=FdmAAAClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
