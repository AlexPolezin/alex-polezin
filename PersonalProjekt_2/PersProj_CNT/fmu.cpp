//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include<stdlib.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buToSpisokClick(TObject *Sender)
{
    tc->ActiveTab = tiSpisok;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCloseClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	tc->ActiveTab = tiAbout;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSpisokClick(TObject *Sender)
{
	tc->ActiveTab = tiSpisok;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------



void __fastcall Tfm::lvSpisokClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiCity->Index);
	laName->Text = dm->cdsCitysNAME->Value;
	meText->Text = dm->cdsCitysDISCIPTION->Value;
	laYears->Text = dm->cdsCitysDATE->Value;
	imCity->Bitmap->Assign(dm->cdsCitysIMG);
}
//---------------------------------------------------------------------------
