//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiSpisok;
	TTabItem *tiCity;
	TListView *lvSpisok;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TToolBar *tbSpiok;
	TButton *buBack;
	TLabel *laGoverCity;
	TLayout *lyMenu;
	TGridPanelLayout *gpMenu;
	TButton *buToSpisok;
	TButton *buRand;
	TButton *buAbout;
	TButton *buClose;
	TTabItem *tiAbout;
	TToolBar *ToolBar1;
	TButton *buSpisok;
	TLabel *laName;
	TToolBar *ToolBar2;
	TButton *buMenu;
	TLabel *laAbout;
	TStyleBook *sbForm;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout1;
	TImage *imCity;
	TLayout *Layout2;
	TLabel *laYear;
	TLabel *laYears;
	TMemo *meText;
	TLabel *Label1;
	TGridPanelLayout *gpMain;
	TLabel *laCIty;
	TImage *imgMain;
	void __fastcall buToSpisokClick(TObject *Sender);
	void __fastcall buCloseClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buSpisokClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvSpisokClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
