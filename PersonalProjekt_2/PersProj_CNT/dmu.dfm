object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'DriverName=DataSnap'
      'HostName=localhost'
      'port=211'
      'Filters={}')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmAAA'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 48
    Top = 96
  end
  object cdsCitys: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCitys'
    RemoteServer = DSProviderConnection1
    Left = 48
    Top = 152
    object cdsCitysID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsCitysNAME: TWideStringField
      FieldName = 'NAME'
      Size = 400
    end
    object cdsCitysDATE: TIntegerField
      FieldName = 'DATE'
    end
    object cdsCitysDISCIPTION: TWideStringField
      FieldName = 'DISCIPTION'
      Size = 20000
    end
    object cdsCitysIMG: TBlobField
      FieldName = 'IMG'
      Size = 1
    end
  end
end
