//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Types.hpp>
#include <FMX.Types3D.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Layers3D.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TViewport3D *Viewport3D1;
	TSphere *Sphere1;
	TLightMaterialSource *LightMaterialSource1;
	TLight *Light1;
	TDummy *Dummy1;
	TFloatAnimation *FloatAnimation1;
	TDummy *Dummy2;
	TSphere *Sphere2;
	TFloatAnimation *FloatAnimation2;
	TLightMaterialSource *LightMaterialSource2;
	TImage3D *Image3D1;
	TDummy *Dummy3;
	TSphere *Sphere3;
	TFloatAnimation *FloatAnimation3;
	TLightMaterialSource *LightMaterialSource3;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
